﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    public float accuracy = 1.0f;
    public Transform playerPos;
    GameObject[] agents;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    private void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    RaycastHit hit;
        //    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000));
        //    {
        //        foreach (GameObject ai in agents)
        //        {
        //            ai.GetComponent<AIControl>().agent.SetDestination(hit.point);
        //        }
        //    }
        //}

        foreach (GameObject ai in agents)
        {
            ai.GetComponent<AIControl>().agent.SetDestination(playerPos.position);
        }
    }
}
