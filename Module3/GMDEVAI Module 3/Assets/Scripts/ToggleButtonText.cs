﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleButtonText : MonoBehaviour
{

    public Text text;
    public bool isToggledOn = false;

    private void Start()
    {
        if (isToggledOn)
        {
            text.text = "Movement: ON";
        }
        else
        {
            text.text = "Movement: OFF";
        }
    }

    public void changeText()
    {
        if (isToggledOn)
        {
            text.text = "Movement: OFF";
            isToggledOn = false;
        }
        else
        {
            text.text = "Movement: ON";
            isToggledOn = true;
        }
    }
}
