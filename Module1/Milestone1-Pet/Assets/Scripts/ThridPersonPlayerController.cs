﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThridPersonPlayerController : MonoBehaviour
{
    //Reference: https://www.youtube.com/watch?v=7nxpDwnU0uU
    public float speed = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        PlayerMovement();
    }

    void PlayerMovement()
    {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 playerMovement = new Vector3(hor, 0f, ver) * speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
    }
}
