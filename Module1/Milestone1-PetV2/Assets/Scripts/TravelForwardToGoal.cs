﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelForwardToGoal : MonoBehaviour
{
    public Transform goal;
    public float speed = 0.1f;
    public float rotSpeed = 0.1f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 endPos = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);

        Vector3 direction = endPos - transform.position;

        transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);

        if (Vector3.Distance(endPos, transform.position) > 1)
            transform.position = Vector3.Slerp(transform.position, endPos, Time.deltaTime * speed);
            //transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime * speed);
            //transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
