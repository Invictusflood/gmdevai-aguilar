﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControlEvade : AIControl
{
    void Evade()
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;

        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);

        Flee(target.transform.position + target.transform.forward * lookAhead);
    }

    protected override void Move()
    {
        Evade();
    }
}
