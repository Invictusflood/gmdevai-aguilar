﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControlCleverHide : AIControl
{
    //void Hide()
    //{
    //    float distance = Mathf.Infinity;
    //    Vector3 chosenSpot = Vector3.zero;

    //    int hidingSpotsCount = World.Instance.GetHidingSpots().Length;

    //    for (int i = 0; i < hidingSpotsCount; i++)
    //    {
    //        Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
    //        Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5;

    //        float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
    //        if (spotDistance < distance)
    //        {
    //            chosenSpot = hidePosition;
    //            distance = spotDistance;
    //        }
    //    }
    //    Seek(chosenSpot);

    //}
    void CleverHide()
    {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;
        Vector3 chosenDir = Vector3.zero;
        GameObject chosenGameObject = World.Instance.GetHidingSpots()[0];

        int hidingSpotsCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < hidingSpotsCount; i++)
        {
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; //distance offset

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                chosenDir = hideDirection;
                chosenGameObject = World.Instance.GetHidingSpots()[i];
                distance = spotDistance;
            }
        }

        Collider hideCol = chosenGameObject.GetComponentInChildren<Collider>();
        Ray back = new Ray(chosenSpot, -chosenDir.normalized);
        RaycastHit info;
        float rayDistance = 100.0f;//must be bigger than distance offset
        hideCol.Raycast(back, out info, rayDistance);

        Seek(info.point + chosenDir.normalized * 5);

    }

    bool canSeeTarget()
    {
        RaycastHit raycastInfo;
        Vector3 rayToTarget = target.transform.position - this.transform.position;
        if (Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo))
        {
            return raycastInfo.transform.gameObject.tag == "Player";
        }
        return false;
    }
    protected override void Move()
    {
        if (canSeeTarget())
        {
            CleverHide();
        }
    }
}
