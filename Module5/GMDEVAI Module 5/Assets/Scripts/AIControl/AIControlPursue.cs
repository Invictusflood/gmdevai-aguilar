﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControlPursue : AIControl
{
    void Pursue()
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;

        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);

        Seek(target.transform.position + target.transform.forward * lookAhead);
    }

    protected override void Move()
    {
        Pursue();
    }
}
