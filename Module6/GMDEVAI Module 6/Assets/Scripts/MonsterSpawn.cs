﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawn : MonoBehaviour
{
    public GameObject obsticle;
    public GameObject desireable;
    GameObject[] agents;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
    }

    void SpawnGameObject(GameObject spawnObject)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction, out hit))
        {
            Instantiate(spawnObject, hit.point, spawnObject.transform.rotation);
            foreach (GameObject a in agents)
            {
                if (spawnObject == obsticle)
                    a.GetComponent<AIControl>().DetectNewObstacle(hit.point);
                else if (spawnObject == desireable)
                    a.GetComponent<AIControl>().DetectNewDesireable(hit.point);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SpawnGameObject(obsticle);
        }

        if (Input.GetMouseButtonDown(1))
        {
            SpawnGameObject(desireable);
        }
    }
}
