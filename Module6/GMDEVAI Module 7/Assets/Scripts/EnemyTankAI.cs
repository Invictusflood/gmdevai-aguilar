﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankAI : TankBaseAI
{

    Animator anim;
    public GameObject player;

    public GameObject GetPlayer()
    {
        return player;
    }
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        anim.SetFloat("healthPercentage", ((float)health / (float)maxHealth) * 100.0f);
    }

    public void StopFiring()
    {
        CancelInvoke("Fire");
    }
    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }
}
