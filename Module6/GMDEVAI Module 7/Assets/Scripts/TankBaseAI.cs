﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBaseAI : MonoBehaviour
{
    public GameObject bullet;
    public GameObject turret;
    public int health, maxHealth;

    protected void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }

    protected void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Bullet")
        {
            TakeDamage();
        }
    }

    protected void TakeDamage()
    {
        health--;
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
